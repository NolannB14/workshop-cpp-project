cmake_minimum_required(VERSION 3.17)

project(r-type_client VERSION 0.0.1 LANGUAGES CXX)

###############################################################################
# CONFIGURATION
###############################################################################

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/../)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../cmake)
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${PROJECT_NAME})

option(TESTING "Unit tests" OFF)

###############################################################################
# DEPENDENCIES
###############################################################################

find_package(sfml COMPONENTS graphics window system audio REQUIRED)
find_package(optional 1.0.0 REQUIRED)
find_package(expected 1.0.0 REQUIRED)
find_package(fmt REQUIRED)

###############################################################################
# SET COMPILATION FLAGS
###############################################################################
if (CMAKE_COMPILER_IS_GNUCXX)
	set(CMAKE_CXX_FLAGS_DEBUG "-Wall -Wextra -Wno-unused-parameter -W -g")
endif()
if (NOT WIN32 AND NOT WIN64)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3")
endif()

set(CMAKE_CXX_FLAGS_RELEASE "-O3")

###############################################################################
# RESSOURCES
###############################################################################
set(PROJECT_SOURCES
    src/Audio/AudioError.cpp
    src/Audio/Music.cpp
    src/Audio/SoundPlayer.cpp
    src/ECS/ECSErrors.cpp
    src/ECS/Core/Bitset.cpp
    src/ECS/Core/Entity.cpp
    src/ECS/Core/EntitiesManager.cpp
    src/ECS/Core/ASystem.cpp
    src/ECS/Core/SystemsManager.cpp
    src/ECS/Core/Coordinator.cpp
    src/ECS/Components/RenderableImage.cpp
    src/ECS/Components/Animation.cpp
    src/ECS/Components/Collider.cpp
    src/ECS/Components/Area.cpp
    src/ECS/Systems/ImageRenderSystem.cpp
    src/ECS/Systems/ImageBoundsSystem.cpp
    src/ECS/Systems/AreaBoundsSystem.cpp
    src/ECS/Systems/BoundsPreviewSystem.cpp
    src/ECS/Systems/TransformSystem.cpp
    src/ECS/Systems/AnimationSystem.cpp
    src/ECS/Systems/CollisionSystem.cpp
    src/ECS/Systems/ParallaxSystem.cpp
    src/Game/Bullet.cpp
    src/Game/BorderWalls.cpp
    src/Game/Enemy.cpp
    src/Game/Explosion.cpp
    src/Game/Parallax.cpp
    src/Game/Player.cpp
    src/Game/UpdateDataFromNetwork.cpp
    src/Graphic/Window.cpp
    src/Graphic/Renderer.cpp
    src/Graphic/EventHandler.cpp
    src/Graphic/KeyEventController.cpp
    src/Graphic/RessourceManager.cpp
    src/Network/BinaryConverter.cpp
    src/Network/ClientNetwork.cpp
    src/Network/NetworkError.cpp
    src/Network/NetworkSerialiser.cpp
    src/Utils/Callback.cpp
    src/Utils/Clock.cpp
    src/Utils/Randomizer.cpp
    src/Utils/Utils.cpp
)
set(LIBRARIES
    sfml-system
    sfml-graphics
    sfml-window
    sfml-audio
    sfml-network
    fmt::fmt
)
set(INCLUDES
    include/
)

###############################################################################
# ADDING INCLUDES FROM DEPENDENCIES
###############################################################################
# -------------------------------- OPTIONAL -----------------------------------
if(optional_FOUND)
    get_optional_include_property(OPTIONAL_INCLUDES)
    if(value_OPTIONAL_INCLUDES)
        set(INCLUDES
            ${INCLUDES}
            ${value_OPTIONAL_INCLUDES}
        )
    endif(value_OPTIONAL_INCLUDES)
endif(optional_FOUND)
# -------------------------------- EXPECTED -----------------------------------
if(expected_FOUND)
    get_expected_include_property(EXPECTED_INCLUDES)
    if(value_EXPECTED_INCLUDES)
        set(INCLUDES
            ${INCLUDES}
            ${value_EXPECTED_INCLUDES}
        )
    endif(value_EXPECTED_INCLUDES)
endif(expected_FOUND)


###############################################################################
# CREATE EXECUTABLE
###############################################################################
add_executable(${PROJECT_NAME}
    src/Main.cpp
    ${PROJECT_SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
    ${INCLUDES}
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE
    ${LIBRARIES}
)

###############################################################################
# BUILD TESTS
###############################################################################

set(TESTS_NAME unit_tests_client)
set(TESTS_RESSOURCES
    tests/StartTests.cpp
    tests/TestTests.cpp
)
set(TESTS_LIBRARIES
    GTest::gtest_main
)

if (TESTING)
    add_executable(${TESTS_NAME}
        ${PROJECT_RESSOURCES}
        ${TESTS_RESSOURCES}
    )

    target_include_directories(${TESTS_NAME}
        PRIVATE
        ${INCLUDES}
    )

    target_link_libraries(${TESTS_NAME}
        PRIVATE
        ${LIBRARIES}
        ${TESTS_LIBRARIES}
    )

    include(FetchContent)
    FetchContent_Declare(
      googletest
      GIT_REPOSITORY https://github.com/google/googletest.git
      GIT_TAG release-1.12.1
    )

    # For Windows: Prevent overriding the parent project's compiler/linker settings
    set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

    FetchContent_MakeAvailable(googletest)

    include(GoogleTest)

    enable_testing()
    gtest_discover_tests(${TESTS_NAME})
endif()
