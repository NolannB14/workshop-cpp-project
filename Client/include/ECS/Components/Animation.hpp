#pragma once

#include "SFML/Graphics/Sprite.hpp"
#include "SFML/System/Time.hpp"
#include "SFML/System/Vector2.hpp"
#include "Utils/Callback.hpp"

#include <optional>

namespace ecs::component {

class Animation {
   public:
    Animation() noexcept = delete;
    Animation(unsigned int frameNumber, sf::Vector2i frameSize, float frameDuration, bool loop = false, unsigned int startingFrame = 1, std::optional<utils::Callback> callback = std::nullopt);
    Animation(Animation const& other) noexcept = delete;
    Animation(Animation&& other) noexcept = default;
    ~Animation() noexcept = default;

    Animation& operator=(Animation const& other) noexcept = delete;
    Animation& operator=(Animation&& other) noexcept = default;

    Animation& looping(bool looping = true) noexcept;
    Animation& reverse(bool reverse = true) noexcept;
    Animation& setFrameDuration(float newDuration);
    Animation& setFrameNumber(unsigned int newFrameNumber);
    Animation& setFrameSize(sf::Vector2i newFrameSize);
    Animation& goToStart() noexcept;
    Animation& goToEnd() noexcept;
    Animation& start() noexcept;
    Animation& stop();
    bool checkNextFrame(float elapsedTime);
    sf::IntRect getNewArea() const noexcept;
    bool isEnded() const noexcept;
    Animation& setCallback(utils::Callback&& callback) noexcept;
    Animation& removeCallback() noexcept;

   private:
    unsigned int frameNumber;
    int currentFrame;
    unsigned int frameDuration;
    sf::Vector2i frameSize;
    std::optional<utils::Callback> callback;
    bool loop = false;
    bool reversing = false;
    bool running = false;
    bool upToDate = true;
    float currentFrameDuration = 0;
};

} // namespace ecs::component