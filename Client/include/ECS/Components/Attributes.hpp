/**
 * @file Attributes.hpp
 * @brief Atributes component to set value for Entity
 * @author Baptiste-MV
 * @version 1
 */

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#pragma once

namespace ecs::component {
/**
 * @struct Attributes
 * @brief Structure Attributes contains position, scale, angle for the Entity
 */
struct Attributes {
    /**
     * @var position
     * @brief The position x and y of the Entity
     */
    sf::Vector2f position{0, 0};
    /**
     * @var scale
     * @brief The scale x and y of the Entity
     */
    sf::Vector2f scale{1, 1};
    /**
     * @var angle
     * @brief The angle degrees of the Entity
     */
    float angle{0};
};

} // namespace ecs::component