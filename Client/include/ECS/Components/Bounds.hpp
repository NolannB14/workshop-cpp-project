/**
 * @file Bounds.hpp
 * @brief Atributes component to set bounds Entity
 * @author Baptiste-MV
 * @version 1
 */

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

#pragma once

namespace ecs::component {
/**
 * @var Bounds
 * @brief Bounds is rectangle define bounds of Entity
 */
struct Bounds {
    sf::FloatRect bounds;
    sf::Vector2f transform;
};


} // namespace ecs::component
