/**
 * @file ASystem.hpp
 * @brief System abstraction for ECS systems
 * @pre This is a precondition
 * @warning This is a warning
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "Entity.hpp"

#include <set>

namespace ecs::core {

class Coordinator;

/**
 * @class ASystem
 * @brief ECS system abstraction
 * @details All ECS working systems should inherit from this class
 */
class ASystem {
   public:
    ASystem() noexcept = delete;
    explicit ASystem(Coordinator& coordinator) noexcept;
    ASystem(ASystem const& other) noexcept = delete;
    ASystem(ASystem&& other) noexcept = default;
    virtual ~ASystem() noexcept = default;

    ASystem& operator=(ASystem const& other) noexcept = delete;
    ASystem& operator=(ASystem&& other) noexcept = delete;

    /**
     * @brief Link an entity to the system
     *
     * @pre Be sure that before calling this function that the entity ComponentSignature contain the system ComponentSignature.
     *
     * @param entity The entity to link to the system
     */
    virtual void addEntity(Entity& entity) noexcept;
    /**
     * @brief Unlink an entity from the system
     *
     * @param entity The entity to unlink from the system
     */
    virtual void removeEntity(Entity& entity) noexcept;

   protected:
    /**
     * @brief List of entities linked to the system
     */
    // NOLINTNEXTLINE
    std::vector<std::size_t> entities;
    /**
     * @var coordinator
     * @brief The ECS coordinator the system is associated with
     */
    ecs::core::Coordinator& coordinator;
};

} // namespace ecs::core
