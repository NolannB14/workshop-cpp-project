/**
 * @file Coordinator.hpp
 * @brief The ECS main class to use ECS
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ComponentsManager.hpp"
#include "EntitiesManager.hpp"
#include "SystemsManager.hpp"

/**
 * @namespace ecs::core
 * @brief ECS game engine core build namespace
 */
namespace ecs::core {

/**
 * @class Coordinator
 * @brief The main class of the ECS
 * @details The user should only interact with an object of this class to use the ECS
 */
class Coordinator {
   public:
    Coordinator() noexcept = default;
    Coordinator(Coordinator const& other) noexcept = delete;
    Coordinator(Coordinator&& other) noexcept = default;
    ~Coordinator() noexcept = default;

    Coordinator& operator=(Coordinator const& other) noexcept = delete;
    Coordinator& operator=(Coordinator&& other) noexcept = default;

    /**
     * @brief Create a new entity
     *
     * @return The newly created entity
     */
    Entity createEntity() noexcept;
    /**
     * @brief Destroy the given entity
     *
     * @warning This function make all the references to the given entity invalid
     *
     * @param entity The entity to destroy
     */
    void destroyEntity(Entity& entity) noexcept;

    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    /**
     * @brief Register a new component type in the ECS
     *
     * @pre The T component should already have been registered
     *
     * @throw error::ECSError thrown if the T component has already been registered
     */
    void registerComponent()
    {
        this->componentsManager.registerComponent<T>();
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    Bit getComponentBit() const
    {
        return this->componentsManager.getComponentBit<T>();
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    /**
     * @brief Set the given component for the given entity
     * @details Link this entity with the existing systems
     *
     * @warning The component passed to this function need to already have been constructed
     *
     * @param entity The entity to associate to the component
     * @param component The component to set for the entity
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    T& setComponent(Entity& entity, T const& component)
    {
        this->componentsManager.setComponent<T>(entity, component);
        this->entitiesManager.sign(entity, this->componentsManager.getComponentBit<T>(), true);
        this->systemsManager.entitySignatureChanged(entity, this->entitiesManager.getSignature(entity));

        return this->getComponent<T>(entity);
    }
    template <typename T>
    T& setComponent(Entity& entity, T&& component)
    {
        this->componentsManager.setComponent<T>(entity, std::forward<T>(component));
        this->entitiesManager.sign(entity, this->componentsManager.getComponentBit<T>(), true);
        this->systemsManager.entitySignatureChanged(entity, this->entitiesManager.getSignature(entity));

        return this->getComponent<T>(entity);
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     * @tparam Params The parameters type to pass to the component constructor
     */
    template <typename T, class... Params>
    /**
     * @brief Set and construct the given component for the given entity
     * @details Link this entity with the existing systems
     *
     * @warning The component passed to this function need to already have been constructed
     *
     * @param entity The entity to associate to the component
     * @param params The parameters that will be passed to the component constructor
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    T& setComponent(Entity& entity, Params&&... params)
    {
        this->componentsManager.setComponent<T>(entity, std::forward<Params>(params)...);
        this->entitiesManager.sign(entity, this->componentsManager.getComponentBit<T>(), true);
        this->systemsManager.entitySignatureChanged(entity, this->entitiesManager.getSignature(entity));

        return this->getComponent<T>(entity);
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    /**
     * @brief Get the component associated to the given entity
     *
     * @pre The component need to be set before calling this function
     *
     * @param entity The entity to get the component associated with
     *
     * @return A reference to the component
     *
     * @throw error::ECSError thrown if T component has not been registered
     * @throw error::ECSError thrown if the component of type T has not been setted before calling this function
     */
    T& getComponent(Entity& entity) const
    {
        return this->componentsManager.getComponent<T>(entity);
    }
    template <typename T>
    T& getComponent(std::size_t entity) const
    {
        return this->componentsManager.getComponent<T>(entity);
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    /**
     * @brief Destroy the component associated with the given entity
     * @details Unlink this entity from the existing systems
     *
     * @param entity The entity to destroy the component associated with
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    void eraseComponent(Entity& entity) noexcept
    {
        this->componentsManager.eraseComponent<T>(entity);
        this->entitiesManager.sign(entity, this->componentsManager.getComponentBit<T>(), false);
        this->systemsManager.entitySignatureChanged(entity, this->entitiesManager.getSignature(entity));
    }

    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T, typename... Arg>
    /**
     * @brief Register a new system in the ECS
     *
     * @warning This function should be called only one time for the same system
     *
     * @return A reference to the newly constructed system so the systems updator functions can be called in the code
     *
     * @throw error::ECSError thrown if this function is called more than one time for the same system type
     */
    T& registerSystem()
    {
        auto& system = this->systemsManager.registerSystem<T>(*this);

        ComponentSignature signature;
        ((signature.set(this->getComponentBit<Arg>(), true)), ...);
        this->systemsManager.setSystemSignature<T>(signature);

        return system;
    }

   private:
    /**
     * @brief The ECS entities manager
     */
    EntitiesManager entitiesManager;
    /**
     * @brief The ECS components manager
     */
    ComponentsManager componentsManager;
    /**
     * @brief The ECS systems manager
     */
    SystemsManager systemsManager;
};

} // namespace ecs::core
