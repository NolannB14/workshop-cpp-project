/**
 * @file Parallax.hpp
 * @brief Parallax in the background (manager and background classes)
 * @author Giabibi
 * @version 1
 */

#pragma once

#include "ECS/Core/Coordinator.hpp"

#include <vector>

namespace game {

/**
 * @class ParallaxBackground
 * @brief Moving Background with a specific speed
 */
class ParallaxBackground {
   public:
    ParallaxBackground() noexcept = delete;
    ParallaxBackground(ecs::core::Coordinator& coordinator, const std::string& filepath, unsigned int speed);
    ParallaxBackground(ParallaxBackground const& other) noexcept = delete;
    ParallaxBackground(ParallaxBackground&& other) noexcept;
    ~ParallaxBackground() noexcept;

    ParallaxBackground& operator=(ParallaxBackground const& other) noexcept = delete;
    ParallaxBackground& operator=(ParallaxBackground&& other) noexcept = delete;

   private:
    /**
     * @var background
     * @brief The ECS coordinator
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var background
     * @brief The moving background
     */
    ecs::core::Entity background;
    /**
     * @var speed
     * @brief the current speed of the background
     */
    float speed;
    /**
     * @var valid
     * @brief If the object is valid or not
     */
    bool valid = true;
};

/**
 * @class ParallaxManager
 * @brief Create backgrounds with a specific speed by background
 */
class ParallaxManager {
   public:
    ParallaxManager() noexcept = delete;
    ParallaxManager(ecs::core::Coordinator& coordinator, const std::vector<std::string>& filepaths, unsigned int minSpeed, unsigned int maxSpeed) noexcept;
    ParallaxManager(ParallaxManager const& other) noexcept = delete;
    ParallaxManager(ParallaxManager&& other) noexcept = default;
    ~ParallaxManager() noexcept = default;

    ParallaxManager& operator=(ParallaxManager const& other) noexcept = delete;
    ParallaxManager& operator=(ParallaxManager&& other) noexcept = delete;

   private:
    /**
     * @var background
     * @brief The moving background
     */
    std::vector<ParallaxBackground> backgrounds;
    /**
     * @var minSpeed
     * @brief the speed of the first background
     */
    unsigned int minSpeed;
    /**
     * @var maxSpeed
     * @brief the speed of the last background
     */
    unsigned int maxSpeed;
};

} // namespace game
