/**
 * @file Player.hpp
 * @brief Player file
 * @author Baptiste-MV
 * @version 1
 */

#pragma once

#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Collider.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "Game/Bullet.hpp"
#include "Game/UpdateDataFromNetwork.hpp"
#include "Graphic/Renderer.hpp"
#include "Graphic/Window.hpp"
#include "Network/ClientNetwork.hpp"

namespace game {
/**
 * @class Player
 * @brief class to create enemy that cannot shot
 */
class Player {
   public:
    Player() noexcept = delete;
    /**
     * @brief Create a new player
     *
     * @param coordinator The ECS coordinator
     * @param bulletManager The manager for creating bullets
     * @param window The graphic window
     * @param pos The player position
     */
    Player(ecs::core::Coordinator& coordinator, game::BulletManager& bulletManager, graphic::Window& window, sf::Vector2f pos, network::ClientNetwork& client);
    Player(Player const& other) noexcept = delete;
    Player(Player&& other) noexcept = delete;
    ~Player() noexcept = default;

    Player& operator=(Player const& other) noexcept = delete;
    Player& operator=(Player&& other) noexcept = delete;

    /**
     * @brief start move up player
     *
     * @param coordinator The ECS coordinator
     * @param moving Set to `true` if the entity should move
     */
    void moveUp(ecs::core::Coordinator& coordinator, bool moving = true);
    /**
     * @brief start move down player
     *
     * @param coordinator The ECS coordinator
     * @param moving Set to `true` if the entity should move
     */
    void moveDown(ecs::core::Coordinator& coordinator, bool moving = true);
    /**
     * @brief start move right player
     *
     * @param coordinator The ECS coordinator
     * @param moving Set to `true` if the entity should move
     */
    void moveRight(ecs::core::Coordinator& coordinator, bool moving = true);
    /**
     * @brief start move left player
     *
     * @param coordinator The ECS coordinator
     * @param moving Set to `true` if the entity should move
     */
    void moveLeft(ecs::core::Coordinator& coordinator, bool moving = true);
    /**
     * @brief shot bullet entity
     *
     * @param coordinator The ECS coordinator
     * @param moving Set to `true` if the entity should move
     */
    void stopMoving(ecs::core::Coordinator& coordinator);
    /**
     * @brief Shot a bullet
     *
     * @param coordinator The ECS coordinator
     */
    void shot(ecs::core::Coordinator& coordinator);
    /**
     * @brief Get the player entity
     *
     * @return Player entity
     */
    const size_t getEntity() const noexcept;

   private:
    /**
     * @var speed
     * @brief The player speed
     */
    constexpr static int speed = 400;
    /**
     * @var spriteArea
     * @brief The size of the player sprite
     */
    constexpr static sf::Vector2i spriteArea{32, 14};
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the player contains
     */
    constexpr static unsigned int animationFramesNumber = 3;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 200;
    /**
     * @var scale
     * @brief The scale of the player sprite
     */
    constexpr static float scale = 3;
    /**
     * @var shotDelay
     * @brief The minimum delay between two player shots
     */
    constexpr static unsigned int shotDelay = 500;
    /**
     * @var pathUp
     * @brief The filepath to the ship sprite going upwards
     */
    constexpr static std::string_view pathUp = "player/spaceShipUp.png";
    /**
     * @var pathDown
     * @brief The filepath to the ship sprite going downwards
     */
    constexpr static std::string_view pathDown = "player/spaceShipDown.png";

    /**
     * @brief The ECS coordinator
     *
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var bulletManager
     * @brief A reference to the bullet manager
     */
    game::BulletManager& bulletManager;
    /**
     * @brief Entity player
     *
     */
    ecs::core::Entity player;
    /**
     * @var shotClock
     * @brief The clock used to time player shot and limit them
     */
    sf::Clock shotClock;
    /**
     * @var movingUp
     * @brief Boolean that say if the player is moving upwards or not
     */
    bool movingUp = false;
    /**
     * @var movingDown
     * @brief Boolean that say if the player is moving downwards or not
     */
    bool movingDown = false;
    /**
     * @var movingLeft
     * @brief Boolean that say if the player is moving to the left or not
     */
    bool movingLeft = false;
    /**
     * @var movingUp
     * @brief Boolean that say if the player is moving to the right or not
     */
    bool movingRight = false;
    /**
     * @var id
     * @brief The player id
     */
    unsigned short id;
    /**
     * @var client
     * @brief The player network
     */
    network::ClientNetwork& client;
};

} // namespace game