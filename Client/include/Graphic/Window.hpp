/**
 * @file Window.hpp
 * @brief Window class encapsulation for SFML window
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "EventHandler.hpp"
#include "Renderer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <string>

/**
 * @namespace graphic
 * @brief This namespace contains all the graphics related functions
 */
namespace graphic {

/**
 * @class Window
 * @brief Encapsulated window class to interact with the graphic window
 */
class Window {
   public:
    Window() noexcept = delete;
    /**
     * @brief Init the window
     *
     * @warning Using this constructor will make the newly created window use the default desktop parameters
     *
     * @param title The title of the window
     * @param coordinator The ECS coordinator
     */
    explicit Window(ecs::core::Coordinator& coordinator, const std::string& title = "Window") noexcept;
    /**
     * @brief Init the window
     *
     * @param coordinator The ECS coordinator
     * @param width The width of the window
     * @param height The height of the window
     * @param title The title of the window
     * @param bitsPerPixel The number of bits a unique pixel is constitued of
     */
    Window(ecs::core::Coordinator& coordinator, unsigned int width, unsigned int height, const std::string& title = "Window", unsigned int bitsPerPixel = 32) noexcept;
    Window(Window const& other) noexcept = delete;
    Window(Window&& other) noexcept = delete;
    ~Window() noexcept = default;

    Window& operator=(Window const& other) noexcept = delete;
    Window& operator=(Window&& other) noexcept = delete;

    /**
     * @brief Set if key repeat is enabled or not
     * @details If key repeat is enabled, things as keyboard key pressed event will be send continuously until key is released
     *
     * @param repeat Set to true if a long press on a key should trigger multiple events
     */
    void keyRepeat(bool repeat) noexcept;
    /**
     * @brief Close the graphic window
     *
     * @pre This function should be called only if isOpen return true
     */
    void close() noexcept;
    /**
     * @brief Check if the window is openned
     *
     * @return `false` if the window is closed, `true` otherwise
     */
    bool isOpen() const noexcept;
    /**
     * @brief Render a new frame of the window
     */
    void render() noexcept;
    /**
     * @brief Functions variadic template to take any function pointer with its arguments
     *
     * @tparam F The function signature
     * @tparam Args The arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback that will be triggered everytime the given key will be pressed
     *
     * @param key The key identification that will identify when the callback should be called
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setPressedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        this->eventHandler.setPressedKeyCallback(key, std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear existing pressed callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearPressedKeyCallbacks(sf::Keyboard::Key key) noexcept;
    /**
     * @brief Functions variadic template to take any function pointer with its arguments
     *
     * @tparam F The function signature
     * @tparam Args The arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback that will be triggered everytime the given key will be released
     *
     * @param key The key identification that will identify when the callback should be called
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setReleasedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        this->eventHandler.setReleasedKeyCallback(key, std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear existing released callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearReleasedKeyCallbacks(sf::Keyboard::Key key) noexcept;
    /**
     * @brief Treats events that occured on the window
     *
     * @throw anything that can be thrown by the callbacks functions
     */
    void treatEvents();

   private:
    /**
     * @var window
     * @brief The SFML window
     */
    sf::RenderWindow window;
    /**
     * @var renderer
     * @brief Object displayer
     */
    Renderer renderer;
    /**
     * @var eventHandler
     * @brief The event handler class
     */
    EventHandler eventHandler;
};

} // namespace graphic
