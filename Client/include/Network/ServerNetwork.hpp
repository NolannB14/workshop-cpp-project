/**
 * @file ClientNetwork.hpp
 * @brief The main class for server network
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "NetworkError.hpp"
#include "NetworkSerialiser.hpp"
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/System/Vector2.hpp"
#include "utils/Clock.hpp"

#include <SFML/Network.hpp>
#include <arpa/inet.h>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <vector>

using Byte = std::uint8_t;
/**
 * @namespace Network
 * @brief This is the main namespace for all the network in this RTypes project.
 */

namespace network {

class ClientNetwork {

   public:
    ClientNetwork() noexcept = delete;
    explicit ClientNetwork(unsigned short& serverPort);
    ClientNetwork(ClientNetwork const& other) noexcept = delete;
    ClientNetwork(ClientNetwork&& other) noexcept = delete;
    ClientNetwork& operator=(ClientNetwork const& other) noexcept = delete;
    ClientNetwork& operator=(ClientNetwork&& other) noexcept = delete;
    ~ClientNetwork() noexcept = default;

    void startNetwork();

    void updatePlayer(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    void updateEnemy(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage = 10) noexcept;
    void updateProjectile(unsigned short id, sf::Vector2f pos, unsigned short damage) noexcept;

    void createPlayer(playerInfo& player) noexcept;
    void createEnemy(enemyInfo& enemy) noexcept;
    void createProjectile(projectile& projec) noexcept;

    void deleteEnemy(enemyInfo& enemy) noexcept;
    void deleteProjectile(projectile& projec) noexcept;

   private:
    sf::UdpSocket socketReceive;
    sf::UdpSocket socketSend;

    NetworkSerialiser networkSerialiser;

    void waitingRoom();

    void sendPlayerID() noexcept;
    void sendDataToAllClient() noexcept;
    void sendLoop() noexcept;

    void receiveData() noexcept;

    std::mutex playerMutex;
    std::vector<playerInfo> players;

    std::vector<playerConnection> playersConnection;

    std::mutex enemyMutex;
    std::vector<enemyInfo> enemys;

    std::mutex projectMutex;
    std::vector<projectile> projectiles;

    utils::Clock sendClock;
    utils::Clock receiveClock;

    std::mutex dataGrammeMutex;
    std::vector<Byte> dataGramme;

    std::jthread sendThread;
    std::jthread receiveThread;

    bool running = true;
};

} // namespace network
