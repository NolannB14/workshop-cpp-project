#include "ECS/Systems/TransformSystem.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Transform.hpp"

ecs::system::TransformSystem::TransformSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::TransformSystem::update(float elapsedTime) const
{
    for (auto const& entity : this->entities) {
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& transform = this->coordinator.getComponent<ecs::component::Transform>(entity);

        attributes.angle += (transform.rotation * elapsedTime) / float{1000};
        attributes.scale += (transform.grow * elapsedTime) / float{1000};
        attributes.position += (transform.movement * elapsedTime) / float{1000};
    }
}