/*
** EPITECH PROJECT, 2022
** RType
** File description:
** Tick
*/

#include "Network/Tick.hpp"

network::Tick::Tick(int tickRate) noexcept
{
    this->tickRate = 1000 / tickRate;
}

bool network::Tick::isTime() noexcept
{

    if (this->clock.isElapsed(this->tickRate)) {
        this->clock.saveTimePoint();
        return true;
    }
    return false;
}

void network::Tick::start() noexcept
{
    this->clock.saveTimePoint();
}
