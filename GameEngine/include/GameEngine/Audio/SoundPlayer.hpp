/**
 * @file SoundPlayer.hpp
 * @brief Sound player object
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "GameEngine/Utils/Singleton.hpp"
#include "SFML/Audio/Sound.hpp"
#include "SFML/Audio/SoundBuffer.hpp"
#include "fmt/core.h"

#include <optional>
#include <unordered_map>
#include <vector>

namespace ge::audio {

/**
 * @class Sound
 * @brief Sound object
 */
class Sound {
   public:
    Sound() noexcept = delete;
    /**
     * @brief Construct a new sound object
     *
     * @param filePath The sound file path
     * @param volume The volume to play the sound at
     *
     * @throw audio::error::SoundError thrown if the file is invalid
     */
    Sound(const std::string& filePath, float volume);
    Sound(Sound const& other) noexcept = delete;
    Sound(Sound&& other) noexcept = default;
    ~Sound() noexcept = default;

    Sound& operator=(Sound const& other) noexcept = delete;
    Sound& operator=(Sound&& other) noexcept = default;

    /**
     * @brief Change the sound volume
     *
     * @param volume The new volume to the for the sound
     */
    void setVolume(float volume) noexcept;
    /**
     * @brief Check if the sound has ended
     *
     * @return `true` if the sound is finished playing, `false` otherwise
     */
    bool isEnded() const noexcept;

   private:
    /**
     * @var sound
     * @brief The sound SFML element
     */
    sf::Sound sound;
};

/**
 * @class SoundPlayer
 * @brief Unique instance object that play sounds
 */
class SoundPlayer final : public ge::utils::Singleton<SoundPlayer> {
   public:
    SoundPlayer() noexcept = delete;
    explicit SoundPlayer(token t) noexcept :
        Singleton(t) { }
    SoundPlayer(SoundPlayer const& other) noexcept = delete;
    SoundPlayer(SoundPlayer&& other) noexcept = delete;
    ~SoundPlayer() noexcept = default;

    SoundPlayer& operator=(SoundPlayer const& other) noexcept = delete;
    SoundPlayer& operator=(SoundPlayer&& other) noexcept = delete;

    /**
     * @brief Play the given sound
     *
     * @param filePath The sound file path
     *
     * @throw audio::error::SoundError thrown if the given file is invalid
     */
    static void playSound(const std::string& filePath);
    /**
     * @brief Set the volume of a specific sound
     *
     * @warning This volume will act only on the specific given file
     *
     * @param filePath The path to the file of the sound we want to change the volume of
     * @param volume The volume to set
     */
    static void setSoundVolume(std::string filePath, float volume) noexcept;
    /**
     * @brief Set the volume of all sounds
     * @details This volume will be used for every sound that does not have a specific volume setted
     *
     * @warning Every sound that got a specific volume setted will not take this volume into account
     *
     * @param volume The volume to set
     */
    static void setSoundsVolume(float volume) noexcept;

   private:
    /**
     * @brief Delete sounds that have ended from the vector
     */
    static void deleteEndedSounds() noexcept;

    /**
     * @var specificVolumes
     * @brief Volumes for specific sounds
     */
    std::unordered_map<std::string, float> specificVolumes;
    /**
     * @var volume
     * @brief General sound volume
     */
    float volume = 100;
    /**
     * @var sounds
     * @brief The sounds that are playing
     */
    std::vector<Sound> sounds;
};

} // namespace ge::audio
