#if defined USE_IMAGE_MODULE && defined USE_COLLISION_MODULE

#include "GameEngine/ECS/Systems/ImageBoundsSystem.hpp"

ge::ecs::system::ImageBoundsSystem::ImageBoundsSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::ImageBoundsSystem::update()
{
    for (auto const& entity : this->entities) {
        auto& sprite = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        bounds.bounds = sprite.getBounds();
    }
}

#endif