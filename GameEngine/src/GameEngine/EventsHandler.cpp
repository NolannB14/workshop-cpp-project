#include "GameEngine/EventsHandler.hpp"

void ge::EventsHandler::clearPressedKeyCallbacks() noexcept
{
    for (auto& callback : this->pressedCallbacks) {
        callback.clear();
    }
}

void ge::EventsHandler::clearPressedKeyCallbackFor(ge::KeyboardKeys key) noexcept
{
    this->pressedCallbacks.at(static_cast<std::size_t>(key)).clear();
}

void ge::EventsHandler::clearPressedKeyCallbackFor(std::initializer_list<ge::KeyboardKeys> keys) noexcept
{
    for (const auto& key : keys) {
        this->clearReleasedKeyCallbackFor(key);
    }
}

void ge::EventsHandler::clearReleasedKeyCallbacks() noexcept
{
    for (auto& callback : this->releasedCallbacks) {
        callback.clear();
    }
}

void ge::EventsHandler::clearReleasedKeyCallbackFor(ge::KeyboardKeys key) noexcept
{
    this->releasedCallbacks.at(static_cast<std::size_t>(key)).clear();
}

void ge::EventsHandler::clearReleasedKeyCallbackFor(std::initializer_list<ge::KeyboardKeys> keys) noexcept
{
    for (const auto& key : keys) {
        this->clearReleasedKeyCallbackFor(key);
    }
}

void ge::EventsHandler::clearEventsCallbacks() noexcept
{
    for (auto& callback : this->eventsCallbacks) {
        callback.clear();
    }
}

void ge::EventsHandler::clearEventCallbackFor(ge::KeyboardKeys event) noexcept
{
    this->eventsCallbacks.at(static_cast<std::size_t>(event)).clear();
}

void ge::EventsHandler::clearEventCallbackFor(std::initializer_list<ge::KeyboardKeys> events) noexcept
{
    for (const auto& event : events) {
        this->clearEventCallbackFor(event);
    }
}

void ge::EventsHandler::treatEvents(const std::vector<Event>& events) noexcept
{
    for (const auto& event : events) {
        this->eventsCallbacks.at(static_cast<std::size_t>(event.eventKey)).safeRun();
        if (event.eventKey == EventsKeys::KeyboardPressed) {
            this->pressedCallbacks.at(static_cast<std::size_t>(event.keyboardKey)).safeRun();
        }
        if (event.eventKey == EventsKeys::KeyboardReleased) {
            this->releasedCallbacks.at(static_cast<std::size_t>(event.keyboardKey)).safeRun();
        }
    }
}
