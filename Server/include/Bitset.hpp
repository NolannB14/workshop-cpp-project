/**
 * @file Bitset.hpp
 * @brief Bitset wrapper to stock bit level combinaison
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <cstdint>
#include <vector>

namespace ecs::core {

/**
 * @brief Byte(8 Bit) on binary level
 */
using Byte = std::uint8_t;
/**
 * @brief Bit on binary level
 */
using Bit = std::size_t;

/**
 * @class Bitset
 * @brief Storage to create binary level combinaison
 */
class Bitset {
   public:
    Bitset() noexcept = default;
    Bitset(Bitset const& other) noexcept = default;
    Bitset(Bitset&& other) noexcept = default;
    ~Bitset() noexcept = default;

    Bitset& operator=(Bitset const& other) noexcept = default;
    Bitset& operator=(Bitset&& other) noexcept = default;

    bool operator!=(const Bitset& other) const;
    bool operator==(const Bitset& other) const;

    /**
     * @brief Set a bit to true or false
     *
     * @param i The index that represents the bit to set
     * @param value The value to set to the i bit
     */
    void set(Bit i, bool value) noexcept;
    /**
     * @brief Get a bit value
     *
     * @param i The index that represents the bit to get value of
     *
     * @return Return the value of the Bit represent by i
     */
    bool get(Bit i) const noexcept;
    /**
     * @brief Check if all the Bits set to true from the given Bitset are set to true on this Bitset too
     * @details If one of the Bitset is longer than the other, the shorter Bitset Bits that are not defined are assumed to be set to false
     *
     * @param other Check if this other Bitset is contained into this Bitset
     *
     * @return true if the Bitset contains the other one, false instead
     */
    bool contains(const Bitset& other) const noexcept;

    /**
     * @fn extractData
     * @brief Extract the bit storage of the class
     *
     * @return the vector of Byte of the class
     */
    std::vector<Byte> extractData() const noexcept;

    /**
     * @fn importData
     * @brief Allow the user to import a vector of Byte.
     *
     * @warning When you import your vector this will replace the bit storage of the class by your data.
     *
     */
    void importData(std::vector<Byte> binaryData) noexcept;

   private:
    /**
     * @var data
     * @brief The storage of Bits values
     */
    std::vector<Byte> data;
};

} // namespace ecs::core
