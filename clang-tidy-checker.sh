#!/bin/bash

set -eo pipefail

NoColor="\033[0m"
CyanColor="\033[0;96m"
RedColor="\033[0;91m"

if [[ "$*" == *"-h"* ]] || [[ "$*" == *"--help"* ]]; then
    echo -e "Usage: $0 [Options]\n"
    echo "Options:"
    echo "  -f, --fix           Automatically fix errors and warning by modifying files when possible according to the configuration."
    echo "  -e, --export FILE   Export code with needed changements applied."
    exit 0
fi

./autobuild.sh "cc"

files=`find . -name "*.cpp" -not -path "./build/*" -not -path "./deps/*" -o -name "*.hpp" -not -path "./build/*" -not -path "./deps/*"`
if [[ -z $files ]]; then
    echo -e "\n${RedColor}No file to check${NoColor}"
    exit 0
fi

if [[ "$*" == *"-e"* ]] || [[ "$*" == *"--export"* ]]; then
    for (( arg=1; arg<=$#; arg++ )); do
        nextarg=$((arg+1))
        if [[ ${!arg} == "-e" ]] || [[ ${!arg} == "--export" ]]; then
            filename=${!nextarg}
            break
        fi
    done
    if [[ -z $filename ]] || [[ ${filename::1} == '-' ]]; then
        echo -e "\n${RedColor}Error: '$filename' Invalid filename${NoColor}"
        exit 1
    fi

    clang-tidy $files > $filename

    echo -e "\n${CyanColor}Successfully exported clang-tidy report in '$filename'${NoColor}"
else
    if [[ "$*" == *"-r"* ]] || [[ "$*" == *"--replace"* ]]; then
        clang-tidy --fix $files

        echo -e "\n${CyanColor}Your files have been automatically fixed${NoColor}"
    else
        clang-tidy $files
    fi
fi
